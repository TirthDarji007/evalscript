# EvalScript

This project evaluates the expresssion passed from a line and prints on the consol.

Ply library is used to build the parser. 

Here are the specifications of operator supported.

Operator             Description

literal     The literals given above.

( expression )      A parenthesized expression.

a \[ b \]     Indexing. B may be any expression.

a \*\* b Exponent Performs exponential (power) calculation on operators = a to the power b. 2**3**4 = 2**(3**4) = 2417851639229258349412352

a * b, a / b           Multiplication and division.

a % b Modulus    (divides left hand operand by right hand operand and returns remainder).

a // b     Floor Division - The division of operands where the result is the quotient in which the digits after the decimal point are removed.

a + b, a - b           Addition and subtraction.

a in b     Evaluates to true if it finds a variable in the specified sequence and false otherwise.

a < b, a <= b, a == b, a <> b, a > b, a >= b Comparison (works for numbers and strings like in python).

not a      Boolean NOT.

a and b  Boolean AND.

a or b     Boolean OR.

The operators have the following semantics:

- Indexing: B must be an integer, a must be either a string or a list. If a is a string, returns the b-th character of the string as a string. If a is a list, returns the b-th element of the list (whatever type it has). The index is zero-based, so the first element is at index 0. If the index is out of bounds, it is a semantic error.

- Addition: A and B must be either both numbers, both strings or both lists. If they are integers or reals, then addition (with standard semantics) is performed. If they are both strings, than string concatenation is performed. If they are both lists, then concatenation of the lists is performed.

- Multiplication, Division and Subtraction: A and B must be integers or reals. For division only, B must not be 0. These are performed using standard multiplication semantics.

- Comparison: A and B must be integers, reals or strings. The two values are compared, and the result is True if the comparison is true, and False if the comparison is false.

- Boolean AND, OR, NOT: A and, if present, B must be booleans. 

- If the line contains a syntax error, you should print out: SYNTAX ERROR.

- A semantic error occurs when the line does not contain a syntax error,

An example input file might look like:  
1 - 2 + 3  
1 2 3  
42 + "Red"  
1 - (2 + 3)  
"Hello" + " " + "SeaWolf."  
[1, 2, 3][0] + 40  

The output from this file should look like:  
2  
SYNTAX ERROR  
SEMANTIC ERROR  
-4  
'Hello SeaWolf.'  
41  

Program will be run with a command like:  
python3 stonyBrookScript.py inputFileName.txt  