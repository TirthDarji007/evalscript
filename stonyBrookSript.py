# Name: Tirth Darji
# ID: 110793364

import sys
import ply.lex as lex
import ply.yacc as yacc
import logging

# debug control variable
debug_on = False


# Record to hold variable in program
variables = {}

# Error handling classs
class SyntaxError(Exception):
    pass


class SemanticError(Exception):
    pass

# setup logging
if debug_on:
    logging.basicConfig(
        level = logging.DEBUG,
        filename = "parselog.txt",
        filemode = "w",
        format = "%(filename)10s:%(lineno)4d:%(message)s"
    )
    log = logging.getLogger()

# Node classes to handle data
class Node:
    def __init__(self):
        print("init node")

    def evaluate(self):
        return 0

    def execute(self):
        return 0


# Node to store number
class NumberNode(Node):
    def __init__(self, v):
        if ('.' in v):
            self.value = float(v)
        else:
            self.value = int(v)

    def evaluate(self):
        return self.value


# Node to do binary operation
class BopNode(Node):
    def __init__(self, op, v1, v2):
        self.v1 = v1
        self.v2 = v2
        self.op = op

    def evaluate(self):
        try:
            if (self.op == '+'):
                return self.v1.evaluate() + self.v2.evaluate()
            elif (self.op == '-'):
                return self.v1.evaluate() - self.v2.evaluate()
            elif (self.op == '*'):
                return self.v1.evaluate() * self.v2.evaluate()
            elif (self.op == '/'):
                return self.v1.evaluate() / self.v2.evaluate()
            elif (self.op == '**'):
                return self.v1.evaluate() ** self.v2.evaluate()
            elif (self.op == '%'):
                return self.v1.evaluate() % self.v2.evaluate()
            elif (self.op == '//'):
                return self.v1.evaluate() // self.v2.evaluate()
            elif (self.op == '<'):
                return self.v1.evaluate() < self.v2.evaluate()
            elif (self.op == '<='):
                return self.v1.evaluate() <= self.v2.evaluate()
            elif (self.op == '=='):
                return self.v1.evaluate() == self.v2.evaluate()
            elif (self.op == '<>' or self.op == '!='):
                return self.v1.evaluate() != self.v2.evaluate()
            elif (self.op == '>'):
                return self.v1.evaluate() > self.v2.evaluate()
            elif (self.op == '>='):
                return self.v1.evaluate() >= self.v2.evaluate()
            elif (self.op == 'and'):
                return self.v1.evaluate() and self.v2.evaluate()
            elif (self.op == 'or'):
                return self.v1.evaluate() or self.v2.evaluate()
            elif (self.op == 'in'):
                return self.v1.evaluate() in self.v2.evaluate()
        except Exception:
            if debug_on: print("semantic error from evaluate")
            raise SemanticError


# Node for index
class IndexNode(Node):
    def __init__(self, l, expr):
        self.l = l
        self.expr = expr

    def evaluate(self):
        try:
            return self.l.evaluate()[self.expr.evaluate()]
        except Exception:
            if debug_on: print("semantic error from index node class")
            raise SemanticError


# Node for block
class BlockNode(Node):
    def __init__(self, s):
        if (s is not None):
            self.sl = [s]
        else:
            self.sl = []

    def execute(self):
        for statement in self.sl:
            statement.execute()


# Node for printing
class PrintNode(Node):
    def __init__(self, v):
        self.value = v

    def execute(self):
        # print(self.value.evaluate())
        val = self.value.evaluate()
        if isinstance(val,str):
            string = "\'" + val + "\'"
            print(string)
        else:
            print(val)



# Node for boolean
class BooleanNode(Node):
    def __init__(self, s):
        self.value = s

    def evaluate(self):
        return self.value


# Node for strings
class StringNode(Node):
    def __init__(self, s):
        self.value = str(s)

    def evaluate(self):
        return self.value


# Node for List
class ListNode(Node):
    def __init__(self, s):
        self.value = s

    def evaluate(self):
        l = []
        for elem in self.value:
            l.append(elem.evaluate())
        return l


# Node to define variable in input
class VariableNode(Node):
    def __init__(self, name):
        self.value = name

    def evaluate(self):
        return variables[self.value]



# ### Lex implementation

# reserved names
reserved = {
    #'print': 'PRINT',
    'in': 'IN',
    'not': 'NOT',
    'and': 'AND',
    'or': 'OR',
    #'if': 'IF',
    #'else': 'ELSE',
    #'while': 'WHILE'
}

# tokens
tokens = ['LPAREN', 'RPAREN',
          # 'LBRACE', 'RBRACE',
          'LBRACKET', 'RBRACKET',
          'NUMBER', 'PLUS', 'MINUS', 'TIMES', 'DIVIDE', 'EXPONENT', 'MOD', 'FLOORDIVIDE',
          'BOOLEAN', 'LT', 'LE', 'EQ', 'NE', 'GT', 'GE', 'NNE',
          'DOUBLESTRING', 'SINGLESTRING',
          'COMMA',
          #'SEMI',
          'ID',
          #'EQUALS',
          'NEWLINE'
          ] + list(reserved.values())
t_LPAREN = r'\('
t_RPAREN = r'\)'
#t_LBRACE = r'\{'
#t_RBRACE = r'\}'
t_LBRACKET = r'\['
t_RBRACKET = r'\]'
t_PLUS = r'\+'
t_MINUS = r'-'
t_TIMES = r'\*'
t_DIVIDE = r'/'
t_EXPONENT = r'\*\*'
t_MOD = r'%'
t_FLOORDIVIDE = r'//'
t_LT = r'<'
t_LE = r'<='
t_EQ = r'=='
t_NE = r'<>'
t_NNE = r'!='
t_GT = r'>'
t_GE = r'>='
t_COMMA = r','
#t_SEMI = r';'
#t_EQUALS = r'='
t_NEWLINE = r'\n'


# get numbers
def t_NUMBER(t):
    r'\d*(\d\.|\.\d)\d*|\d+'
    try:
        t.value = NumberNode(t.value)
    except ValueError:
        print("Integer value too large %d", t.value)
        t.value = 0
    return t


# get boolean values
def t_BOOLEAN(t):
    r'(True|False|true|false)'
    if t.value == 'True' or t.value == 'true':
        t.value = BooleanNode(True)
    elif t.value == 'False' or t.value == 'false' :
        t.value = BooleanNode(False)
    else :
        raise SyntaxError
    #t.value = BooleanNode(t.value == 'True')
    return t


# get single quoted string values
def t_SINGLESTRING(t):
    r'\'[^\']*\''
    t.value = StringNode(t.value[1:-1])
    return t


# get double quoted string values
def t_DOUBLESTRING(t):
    r'\"[^\"]*\"'
    t.value = StringNode(t.value[1:-1])
    return t


# get the id from reserved
def t_ID(t):
    r'[a-zA-Z][a-zA-Z0-9_]*'
    t.type = reserved.get(t.value, 'ID')
    return t


# Ignore space
t_ignore = " \t"


# raise an error
def t_error(t):
    if debug_on: print("t_error @: ",t)
    raise SyntaxError


# build the lexer
if debug_on: lex.lex(debug=True,debuglog=log)
else: lex.lex()

# ### yacc implementation

# set the precedence
precedence = (
    ('left', 'OR'),
    ('left', 'AND'),
    ('left', 'NOT'),
    ('nonassoc', 'LT', 'LE', 'EQ', 'NE', 'NNE','GT', 'GE'),
    ('left', 'IN'),
    ('left', 'PLUS', 'MINUS'),
    ('left', 'FLOORDIVIDE'),
    ('left', 'MOD'),
    ('left', 'TIMES', 'DIVIDE'),
    ('right', 'EXPONENT'),
    ('left', 'LBRACKET')
)


# Every input line is expresssion

def p_expression(t):
    """expression : bool
                  | factor
                  | str
                  | list
                  | variable"""
    t[0] = t[1]


def p_expression_binop(t):
    """expression : expression PLUS expression
                  | expression MINUS expression
                  | expression TIMES expression
                  | expression DIVIDE expression
                  | expression MOD expression
                  | expression FLOORDIVIDE expression
                  | expression EXPONENT expression
                  | expression AND expression
                  | expression OR expression
                  | expression LT expression
                  | expression LE expression
                  | expression EQ expression
                  | expression NE expression
                  | expression NNE expression
                  | expression GT expression
                  | expression GE expression
                  | expression IN expression"""
    t[0] = BopNode(t[2], t[1], t[3])


def p_variable_id(t):
    """variable : ID"""
    t[0] = VariableNode(t[1])


def p_factor_number(t):
    """factor : NUMBER"""
    t[0] = t[1]


def p_factor_expr(t):
    """factor : LPAREN expression RPAREN"""
    t[0] = t[2]


def p_bool_boolean(t):
    """bool : BOOLEAN"""
    t[0] = BooleanNode(t[1].evaluate())
    #t[0] = t[1]


def p_not_bool(t):
    """bool : NOT expression"""
    t[0] = BooleanNode(not t[2].evaluate())


def p_str_string(t):
    """str : DOUBLESTRING
            | SINGLESTRING"""
    t[0] = StringNode(t[1].evaluate())


def p_list(t):
    """list : LBRACKET expression listcontents"""
    t[3].insert(0, t[2])
    t[0] = ListNode(t[3])


def p_list_contents(t):
    """listcontents : COMMA expression listcontents"""
    t[3].insert(0, t[2])
    t[0] = t[3]


def p_list_end(t):
    """listcontents : RBRACKET"""
    t[0] = list()


def p_index(t):
    """expression : expression LBRACKET expression RBRACKET"""
    t[0] = IndexNode(t[1], t[3])


def p_newline(t):
    """expression : expression NEWLINE"""
    t[0] = PrintNode(t[1])


def p_error(t):
    raise SyntaxError

# Build the parser
if debug_on: yacc.yacc(debug=True,debuglog=log)
else: yacc.yacc()

# read from the input file and evaluate expression

infile = open(sys.argv[1],'r')
for line in infile:
    try:

        # Check for new line ending at the end of expresssion
        if line[-1] != '\n':
            if debug_on: print("Not ending properly. Adding new line at the end")
            line = line + '\n'

        lex.input(line)
        if debug_on:
            while True:
                token = lex.token()
                if not token: break
                print(token)
        ast = yacc.parse(line)
        ast.execute()
    except SyntaxError:
        print("SYNTAX ERROR")
    except SemanticError:
        print("SEMANTIC ERROR")
    except Exception:
        if debug_on: print("other exception caught")
        print("SEMANTIC ERROR")
